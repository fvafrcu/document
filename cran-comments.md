Dear CRAN Team,
this is a resubmission of package 'document'. I have added the following changes:

* Removed examples for internal functions from the internal documentation (as
  requested by CRAN).
* Fixed broken links in README.Rmd and DESCRIPTION.
* Moved `get_lines_between_tags()` to package *fritools*.

Please upload to CRAN.
Best, Andreas Dominik

# Package document 4.0.0

Reporting is done by packager version 1.15.0


## Test environments
- R Under development (unstable) (2023-06-03 r84490)
   Platform: x86_64-pc-linux-gnu
   Running under: Devuan GNU/Linux 4 (chimaera)
   0 errors | 0 warnings | 2 notes
- win-builder (devel)  
  Date: Tue, 13 Jun 2023 12:12:31 +0200
  Your package document_4.0.0.tar.gz has been built (if working) and checked for Windows.
  Please check the log files and (if working) the binary package at:
  https://win-builder.r-project.org/0hbwT8724AOT
  The files will be removed after roughly 72 hours.
  Installation time in seconds: 5
  Check time in seconds: 141
  Status: 1 NOTE
  R version 4.3.0 (2023-04-21 ucrt)
   

## Local test results
- RUnit:
    document_unit_test - 1 test function, 0 errors, 0 failures in 1 checks.
- testthat:
    [ FAIL 0 | WARN 11 | SKIP 0 | PASS 26 ]
- Coverage by covr:
    document Coverage: 80.00%

## Local meta results
- Cyclocomp:
     Exceeding maximum cyclomatic complexity of 10 for check_package by 4.
- lintr:
    found 27 lints in 725 lines of code (a ratio of 0.0372).
- cleanr:
    found 2 dreadful things about your code.
- codetools::checkUsagePackage:
    found 13 issues.
- devtools::spell_check:
    found 1 unkown words.
